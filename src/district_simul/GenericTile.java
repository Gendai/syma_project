package district_simul;

import repast.simphony.context.Context;
import repast.simphony.space.grid.Grid;

public abstract class GenericTile {
	
	protected Grid<GenericTile> grid;
	protected Context<GenericTile> context;
	
	public GenericTile(Grid<GenericTile> gridCopy, Context<GenericTile> contextCopy)
	{
		this.grid = gridCopy;
		this.context = contextCopy;
	}

}
