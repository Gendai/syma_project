package district_simul.agents;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;
import java.util.Vector;

import district_simul.utils.GridPoint;
import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.grid.Grid;
import district_simul.GenericTile;


public abstract class Agent extends GenericTile {
	protected static Vector<Vector<GridPoint>> mapCopy = null;
	
	protected GridPoint start;
	protected GridPoint goal;
	protected Stack<GridPoint> path;
	protected ArrayList<?> authorizedTiles;
	
	protected int uniqueId = 0;
	
	public Agent(Grid<GenericTile> gridCopy, Context<GenericTile> contextCopy, GridPoint start, GridPoint goal, int uniqueId)
	{
		super(gridCopy, contextCopy);
		this.grid = gridCopy;
		this.start = start;
		this.goal = goal;
		this.path = new Stack<>();
		this.uniqueId = uniqueId;
		if (mapCopy == null)
			mapCopy = singletonInstanceMapCopy(gridCopy);
	}
	
	public Agent(Grid<GenericTile> gridCopy, Context<GenericTile> contextCopy, GridPoint start, int uniqueId)
	{
		super(gridCopy, contextCopy);
		this.grid = gridCopy;
		this.start = start;
		this.uniqueId = uniqueId;
		if (mapCopy == null)
			mapCopy = singletonInstanceMapCopy(gridCopy);
	}
	
	@ScheduledMethod(start = 1, interval = 1, priority = 1)
	public abstract void nextMove();
	
	protected abstract ArrayList<GridPoint> computeNeighbors(GridPoint current, HashMap<GridPoint, GridPoint> cameFrom);
	
	protected abstract boolean CheckForTrafficLight(GridPoint gp);
	
	protected Stack<GridPoint> computePath()
	{
		return computePath(start.x, start.y, goal.x, goal.y);
	}
	
	protected Stack<GridPoint> computePath(int startx, int starty, int endx, int endy) {
		GridPoint startPath = mapCopy.get(starty).get(startx);
		GridPoint goalPath = mapCopy.get(endy).get(endx);
		
		HashSet<GridPoint> closeSet = new HashSet<GridPoint>();
		HashSet<GridPoint> openSet = new HashSet<GridPoint>();
		openSet.add(startPath);
		
		HashMap<GridPoint, Integer> gScore = new HashMap<GridPoint, Integer>();
		HashMap<GridPoint, Integer> fScore = new HashMap<GridPoint, Integer>();
		gScore.put(startPath, 0);
		fScore.put(startPath, heuristicCost(startPath, goalPath));
		
		HashMap<GridPoint, GridPoint> cameFrom = new HashMap<GridPoint, GridPoint>();
		
		while (! openSet.isEmpty()) {
			GridPoint current = getNextGridPoint(openSet, fScore);
			
			if (current == goalPath)
				return reconstructPath(current, cameFrom);

			openSet.remove(current);
			closeSet.add(current);
			
			for (GridPoint neighbour : computeNeighbors(current, cameFrom)) {
				if (closeSet.contains(neighbour))
					continue;
	            
				if (! openSet.contains(neighbour))
					openSet.add(neighbour);
					
				Integer tentative_gScore = gScore.get(current) + 1;
				Integer gScoreNeighbour = gScore.get(neighbour);
	            if(gScoreNeighbour != null && tentative_gScore >= gScoreNeighbour)
	                continue;

	            cameFrom.put(neighbour, current);
	            gScore.put(neighbour, tentative_gScore);
	            fScore.put(neighbour, gScore.get(neighbour) + heuristicCost(neighbour, goalPath)); 
			}
		}
		return new Stack<GridPoint>();
	}
	
	private Stack<GridPoint> reconstructPath(GridPoint current, HashMap<GridPoint, GridPoint> cameFrom) {
		Stack<GridPoint> path = new Stack<GridPoint>();
		while (current != null) {
			path.push(current);
			current = cameFrom.get(current);
		}
		return path;
	}
	
	//Manhattan distance using norm 1
	protected Integer heuristicCost(GridPoint current, GridPoint dest) {
		return Math.abs(dest.y - current.y) + Math.abs(dest.x - current.x);
	}
	
	protected Boolean isAuthorizedTile(Iterable<GenericTile> tiles) {
		for (GenericTile t : tiles) {
			for (Object auth : authorizedTiles) {
				if (((Class<?>)auth).isInstance(t))
					return true;
			}
		}
		return false;
	}
	
	private GridPoint getNextGridPoint(HashSet<GridPoint> openSet, HashMap<GridPoint, Integer> fScore) {
		Integer score = Integer.MAX_VALUE;
		Integer testScore = 0;
		GridPoint res = null;
		for (GridPoint gridPoint : openSet) {
			testScore = fScore.get(gridPoint);
			if (gridPoint != null && testScore != null && testScore < score) {
				score = testScore;
				res = gridPoint;
			}
		}
		return res;
	}
	
	private static Vector<Vector<GridPoint>> singletonInstanceMapCopy(Grid<GenericTile> grid) {
		Integer w = grid.getDimensions().getWidth();
		Integer h = grid.getDimensions().getHeight();
		Vector<Vector<GridPoint>> map = new Vector<Vector<GridPoint>>(h);
		for (int i = 0; i < h; i++) {
			Vector<GridPoint> line = new Vector<GridPoint>(w);
			for (int j = 0; j < w; j++)
				line.add(new GridPoint(j, i));
			map.add(line);
		}
		return map;
	}
	
	protected Boolean isInGrid(GridPoint gp) {
		return (gp != null
				&& gp.x >= 0
				&& gp.x < grid.getDimensions().getWidth()
				&& gp.y >= 0
				&& gp.y < grid.getDimensions().getHeight());	
	}

	protected Boolean isInGrid(int x, int y) {
		return (x >= 0
				&& x < grid.getDimensions().getWidth()
				&& y >= 0
				&& y < grid.getDimensions().getHeight());	
	}
	
	public Boolean hasPath()
	{
		return !this.path.isEmpty();
	}
	
	public void SetNewPath(GridPoint start, GridPoint goal)
	{
		this.start = start;
		this.goal = goal;
		this.path = this.computePath();
	}
}
