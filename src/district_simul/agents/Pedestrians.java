package district_simul.agents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;

import district_simul.ContextCreator;
import district_simul.GenericTile;
import district_simul.tiles.BusStop;
import district_simul.utils.BusLine.BusStopLoc;
import district_simul.tiles.Crosswalk;
import district_simul.tiles.Sidewalk;
import district_simul.tiles.TrafficLight;
import district_simul.tiles.TrafficLight.LightState;
import district_simul.utils.GridPoint;
import repast.simphony.context.Context;
import repast.simphony.space.grid.Grid;

public class Pedestrians extends Agent{
	
	private boolean waitingBus = false;
	private GridPoint busStopGridPoint;
	private long tickCount = 0;
	
	public Pedestrians(Grid<GenericTile> gridCopy, Context<GenericTile> contextCopy, GridPoint startPath, GridPoint endPath, int uniqueId) {
		super(gridCopy, contextCopy, startPath, endPath, uniqueId);
		this.authorizedTiles = new ArrayList<Class<?>>(){
		private static final long serialVersionUID = 290763458116311930L;
		{
				add(Sidewalk.class);
				add(Crosswalk.class);
		}};
		path = computePath();
		if (path.size() == 0 || path.size() > 10) {
			Stack<GridPoint> busPath = computePathBusTraveller();
			if (busPath.size() > 0 && (path.size() > busPath.size() || path.size() == 0))
				path = busPath;
		}
	}
	
	public Pedestrians(Grid<GenericTile> gridCopy, Context<GenericTile> contextCopy, Stack<GridPoint> sgp, GridPoint start, int uniqueId) {
		super(gridCopy, contextCopy, start, uniqueId);
		this.authorizedTiles = new ArrayList<Class<?>>(){
		private static final long serialVersionUID = 1778633839608532830L;
		{
			add(Sidewalk.class);
			add(Crosswalk.class);
		}};
		path = sgp;
	}
	
	@Override
	protected boolean CheckForTrafficLight(GridPoint gp)
	{
		Iterable<GenericTile> gtI = this.grid.getObjectsAt(gp.x, gp.y);
		for (GenericTile gt : gtI)
		{
			if (gt instanceof Crosswalk)
			{
				Crosswalk cw = (Crosswalk)gt;
				TrafficLight tl = cw.getAssociatedTrafficLight();
				if (tl != null)
					return tl.getLightState().equals(LightState.RED) && tl.TimeToCross();
			}
		}
		return true;
	}
	
	public BusStop GetBusStop(GridPoint gp)
	{
		Iterable<GenericTile> tiles = this.grid.getObjectsAt(gp.x, gp.y);
		for (GenericTile gt : tiles)
			if (gt instanceof BusStop)
				return (BusStop)gt;
		return null;
	}
	
	@Override
	public void nextMove() {
		tickCount++;
		if (path.size() > 0)
		{
			if (waitingBus)
			{
				BusStop bs = GetBusStop(busStopGridPoint);
				for (GenericTile gt : grid.getObjectsAt(bs.GetRoad().x, bs.GetRoad().y))
				{
					if (gt instanceof Bus)
					{
						Bus b = (Bus)gt;
						BusStop bsDest = GetBusStop(path.peek());
						int id = b.GetBusStop(bsDest.GetRoad());
						if (b.OnBoard(this, id))
							this.context.remove(this);
					}
				}
			}
			else
			{
				GridPoint toGo = path.peek();
				if (CheckForTrafficLight(toGo))
				{
					this.grid.moveTo(this, toGo.x, toGo.y);
					path.pop();
					if (toGo.getWaitForBus())
					{
						waitingBus = true;
						busStopGridPoint = toGo;
					}
				}
			}
		}
		else
		{
			this.context.remove(this);
			ContextCreator.agentExecTime.put(this.uniqueId, tickCount);
		}
	}
	
	@Override
	protected ArrayList<GridPoint> computeNeighbors(GridPoint current, HashMap<GridPoint, GridPoint> cameFrom) {
		ArrayList<GridPoint> neighbors = new ArrayList<GridPoint>();
		Integer w = grid.getDimensions().getWidth();
		Integer h = grid.getDimensions().getHeight();
		
		if (current.x - 1 >= 0 && isAuthorizedTile(grid.getObjectsAt((current.x - 1), current.y)))
			neighbors.add(mapCopy.get(current.y).get(current.x - 1));
		if (current.x + 1 < w && isAuthorizedTile(grid.getObjectsAt((current.x + 1), current.y)))
			neighbors.add(mapCopy.get(current.y).get(current.x + 1));
		if (current.y - 1 >= 0 && isAuthorizedTile(grid.getObjectsAt(current.x, (current.y - 1))))
			neighbors.add(mapCopy.get(current.y - 1).get(current.x));
		if (current.y + 1 < h && isAuthorizedTile(grid.getObjectsAt(current.x, (current.y + 1))))
			neighbors.add(mapCopy.get(current.y + 1).get(current.x));
		return neighbors;
	}
	
	
	private Stack<GridPoint> computePathBusTraveller() {
		HashMap<Integer, TreeMap<Integer, HashSet<GridPoint>>> busStopsStart = sortBusStops(start);
		HashMap<Integer, TreeMap<Integer, HashSet<GridPoint>>> busStopsGoal = sortBusStops(goal);
		
		Integer bestDM = Integer.MAX_VALUE;
		HashMap<GridPoint, Stack<GridPoint>> store_astar_goal = new HashMap<GridPoint, Stack<GridPoint>>();
		Stack<GridPoint> gp_stop_start_path = null;
		Stack<GridPoint> gp_stop_goal_path = null;
		GridPoint gp_stop_start = null;
		GridPoint gp_stop_goal = null;
		
		for (Integer busLineStart : busStopsStart.keySet()) {
			Boolean break_loop = false;
			for (Map.Entry<Integer, HashSet<GridPoint>> dist_set_start : busStopsStart.get(busLineStart).entrySet()) {
				if (! break_loop) {
					for (Map.Entry<Integer, HashSet<GridPoint>> dist_set_goal : busStopsGoal.get(busLineStart).entrySet()) {
						if (dist_set_start.getKey() + dist_set_goal.getKey() < bestDM)
						{
							for (GridPoint stop_start : dist_set_start.getValue()) {
								Stack<GridPoint> startPath = computePath(start.x, start.y, stop_start.x, stop_start.y);
								if (startPath.size() == 0)
									continue;
								for (GridPoint stop_goal : dist_set_goal.getValue()) {
									Stack<GridPoint> goalPath;
									if (store_astar_goal.containsKey(stop_goal))
										goalPath = store_astar_goal.get(stop_goal);
									else
										goalPath = computePath(stop_goal.x, stop_goal.y, goal.x, goal.y);
									if (goalPath.size() == 0)
										store_astar_goal.put(stop_goal, goalPath);
									else {
										gp_stop_start = stop_start;
										gp_stop_goal = stop_goal;
										gp_stop_start_path = startPath;
										gp_stop_goal_path = goalPath;
										bestDM = dist_set_start.getKey() + dist_set_goal.getKey();
										break_loop = true;
										break;
									}
								}
							}
						}
						else {
							break_loop = true;
							break;
						}
					}
				}
				else
					break;
			}
		}
		
		if (gp_stop_start != null && gp_stop_goal != null) {
			Stack<GridPoint> path = new Stack<GridPoint>();
			Stack<GridPoint> rev_path = new Stack<GridPoint>();
			while (gp_stop_start_path.size() > 1)
				rev_path.push(gp_stop_start_path.pop());
			
			if (gp_stop_start_path.peek() == gp_stop_goal_path.peek()) {
				gp_stop_goal_path.pop();
				rev_path.push(gp_stop_start_path.pop());
			}
			else {
				GridPoint stop = gp_stop_start_path.pop().Copy();
				stop.setWaitForBus(true);
				rev_path.push(stop);
				rev_path.push(gp_stop_goal_path.pop());
			}
				
			while (! gp_stop_goal_path.empty())
				rev_path.push(gp_stop_goal_path.pop());
			while (! rev_path.empty())
				path.push(rev_path.pop());
			return path;
		}
		return new Stack<GridPoint>();
	}
	
	private HashMap<Integer, TreeMap<Integer, HashSet<GridPoint>>> sortBusStops(GridPoint from) {
		HashMap<Integer, TreeMap<Integer, HashSet<GridPoint>>> stops = new HashMap<Integer, TreeMap<Integer, HashSet<GridPoint>>>();
		for (Integer line : ContextCreator.busLines.keySet()) {
			TreeMap<Integer, HashSet<GridPoint>> treemap = new TreeMap<Integer, HashSet<GridPoint>>();
			for (BusStopLoc stop : ContextCreator.busLines.get(line).getBusStops()) {
				int dist = heuristicCost(from, stop.gridPoint);
				if (! treemap.containsKey(dist))
					treemap.put(dist, new HashSet<GridPoint>());
				treemap.get(dist).add(stop.gridPoint);
			}
			stops.put(line, treemap);
		}
		
		return stops;
	}
	
	public Stack<GridPoint> getPath()
	{
		return path;
	}
}
