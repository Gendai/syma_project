package district_simul.agents;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bibliothek.util.container.Tuple;
import district_simul.ContextCreator;
import district_simul.GenericTile;
import district_simul.tiles.Crosswalk;
import district_simul.tiles.HorizontalCrosswalk;
import district_simul.tiles.Road;
import district_simul.tiles.Road.Direction;
import district_simul.tiles.TrafficLight;
import district_simul.tiles.TrafficLight.LightState;
import district_simul.tiles.VerticalCrosswalk;
import district_simul.utils.GridPoint;
import repast.simphony.context.Context;
import repast.simphony.space.grid.Grid;

public class Vehicle extends Agent
{
	protected boolean recomputePath = false;
	protected boolean goingToMove = false;
	
	protected long tickCount = 0;
	
	private final static Map<String, List<GridPoint>> mapRoadNeighbor = new HashMap<String, List<GridPoint>>();
	{
		mapRoadNeighbor.put("1_dl_sw", Arrays.asList(new GridPoint(-1, 0)));
		mapRoadNeighbor.put("1_dr_se", Arrays.asList(new GridPoint(1, 0)));
		mapRoadNeighbor.put("1_h_e", Arrays.asList(new GridPoint(-1, 0)));
		mapRoadNeighbor.put("1_h_w", Arrays.asList(new GridPoint(1, 0)));
		mapRoadNeighbor.put("1_ld_ws", Arrays.asList(new GridPoint(0, -1)));
		mapRoadNeighbor.put("1_lf_wn", Arrays.asList(new GridPoint(0, 1)));
		mapRoadNeighbor.put("1_rd_es", Arrays.asList(new GridPoint(0, -1)));
		mapRoadNeighbor.put("1_ru_en", Arrays.asList(new GridPoint(0, 1)));
		mapRoadNeighbor.put("1_td_swe",Arrays.asList(new GridPoint(-1, 0), new GridPoint(1, 0)));
		mapRoadNeighbor.put("1_td_ws", Arrays.asList(new GridPoint(1, 0), new GridPoint(0, -1)));
		mapRoadNeighbor.put("1_td_es", Arrays.asList(new GridPoint(-1, 0), new GridPoint(0, -1)));
		mapRoadNeighbor.put("1_td_sw", Arrays.asList(new GridPoint(1, 0)));
		mapRoadNeighbor.put("1_td_se", Arrays.asList(new GridPoint(-1, 0)));
		mapRoadNeighbor.put("1_tl_wns",Arrays.asList(new GridPoint(0, 1), new GridPoint(0, -1)));
		mapRoadNeighbor.put("1_tl_sw", Arrays.asList(new GridPoint(-1, 0), new GridPoint(0, -1)));
		mapRoadNeighbor.put("1_tl_nw", Arrays.asList(new GridPoint(-1, 0), new GridPoint(0, 1)));
		mapRoadNeighbor.put("1_tl_wn", Arrays.asList(new GridPoint(0, 1)));
		mapRoadNeighbor.put("1_tl_ws", Arrays.asList(new GridPoint(0, -1)));
		mapRoadNeighbor.put("1_tr_ne", Arrays.asList(new GridPoint(1, 0), new GridPoint(0, 1)));
		mapRoadNeighbor.put("1_tr_wns",Arrays.asList(new GridPoint(0, -1), new GridPoint(0, 1)));
		mapRoadNeighbor.put("1_tr_se", Arrays.asList(new GridPoint(1, 0), new GridPoint(0, -1)));
		mapRoadNeighbor.put("1_tr_ws", Arrays.asList(new GridPoint(0, -1)));
		mapRoadNeighbor.put("1_tr_wn", Arrays.asList(new GridPoint(0, 1)));
	    mapRoadNeighbor.put("1_tu_new",Arrays.asList(new GridPoint(-1, 0), new GridPoint(1, 0)));
		mapRoadNeighbor.put("1_tu_wn", Arrays.asList(new GridPoint(-1, 0), new GridPoint(0, 1)));
		mapRoadNeighbor.put("1_tu_en", Arrays.asList(new GridPoint(1, 0), new GridPoint(0, 1)));
		mapRoadNeighbor.put("1_tu_nw", Arrays.asList(new GridPoint(-1, 0)));
		mapRoadNeighbor.put("1_tu_ne", Arrays.asList(new GridPoint(1, 0)));
		mapRoadNeighbor.put("1_ul_nw", Arrays.asList(new GridPoint(-1, 0)));
		mapRoadNeighbor.put("1_ur_ne", Arrays.asList(new GridPoint(1, 0)));
		mapRoadNeighbor.put("1_v_n", Arrays.asList(new GridPoint(0, 1)));
		mapRoadNeighbor.put("1_v_s", Arrays.asList(new GridPoint(0, -1)));
		mapRoadNeighbor.put("1_sq_nsew", Arrays.asList(new GridPoint(-1, 0), new GridPoint(1, 0), new GridPoint(0, -1), new GridPoint(0, 1)));
	}

	private final static Map<Tuple<Integer, Direction>, List<GridPoint>> mapCrossroadNeighbor = new HashMap<Tuple<Integer, Direction>, List<GridPoint>>();
	{
		mapCrossroadNeighbor.put(new Tuple<>(1, Direction.NORTH), Arrays.asList(new GridPoint(1, 0), new GridPoint(0, 1)));	
		mapCrossroadNeighbor.put(new Tuple<>(1, Direction.SOUTH), Arrays.asList(new GridPoint(-1, 0), new GridPoint(0, -1)));
		mapCrossroadNeighbor.put(new Tuple<>(1, Direction.EAST), Arrays.asList(new GridPoint(1, 0), new GridPoint(0, -1)));
		mapCrossroadNeighbor.put(new Tuple<>(1, Direction.WEST), Arrays.asList(new GridPoint(-1, 0), new GridPoint(0, 1)));
		
		mapCrossroadNeighbor.put(new Tuple<>(2, Direction.NORTH), Arrays.asList(new GridPoint(-1, 0), new GridPoint(0, 1)));	
		mapCrossroadNeighbor.put(new Tuple<>(2, Direction.SOUTH), Arrays.asList(new GridPoint(1, 0), new GridPoint(0, -1)));
		mapCrossroadNeighbor.put(new Tuple<>(2, Direction.EAST), Arrays.asList(new GridPoint(1, 0), new GridPoint(0, 1)));
		mapCrossroadNeighbor.put(new Tuple<>(2, Direction.WEST), Arrays.asList(new GridPoint(-1, 0), new GridPoint(0, -1)));
		
		mapCrossroadNeighbor.put(new Tuple<>(3, Direction.NORTH), Arrays.asList(new GridPoint(-1, 0), new GridPoint(0, 1)));	
		mapCrossroadNeighbor.put(new Tuple<>(3, Direction.SOUTH), Arrays.asList(new GridPoint(1, 0), new GridPoint(0, -1)));
		mapCrossroadNeighbor.put(new Tuple<>(3, Direction.EAST), Arrays.asList(new GridPoint(1, 0), new GridPoint(0, 1)));
		mapCrossroadNeighbor.put(new Tuple<>(3, Direction.WEST), Arrays.asList(new GridPoint(-1, 0), new GridPoint(0, -1)));
		
		mapCrossroadNeighbor.put(new Tuple<>(4, Direction.NORTH), Arrays.asList(new GridPoint(0, 1)));	
		mapCrossroadNeighbor.put(new Tuple<>(4, Direction.SOUTH), Arrays.asList(new GridPoint(0, -1)));
		mapCrossroadNeighbor.put(new Tuple<>(4, Direction.EAST), Arrays.asList(new GridPoint(1, 0)));
		mapCrossroadNeighbor.put(new Tuple<>(4, Direction.WEST), Arrays.asList(new GridPoint(-1, 0)));
	}
	
	public Vehicle(Grid<GenericTile> gridCopy, Context<GenericTile> contextCopy, GridPoint startPath, GridPoint endPath, int uniqueId) {
		super(gridCopy, contextCopy, startPath, endPath, uniqueId);
		this.authorizedTiles = new ArrayList<Class<?>>(){
			private static final long serialVersionUID = -8218958236107228087L;
		{
			add(Road.class);
			add(Crosswalk.class);
		}};
	}
	
	public boolean GoingToMove()
	{
		return goingToMove;
	}

	@Override
	public void nextMove()
	{
		tickCount++;
		goingToMove = false;
		if (path.size() > 0)
		{
			GridPoint toGo = path.peek();
			if (CheckForTrafficLight(toGo) && CheckForCars(toGo))
			{
				this.grid.moveTo(this, toGo.x, toGo.y);
				goingToMove = true;
				path.pop();
			}
		}
		else
		{
			this.context.remove(this);
			ContextCreator.agentExecTime.put(this.uniqueId, tickCount);
		}
	}
	
	protected ArrayList<GridPoint> computeNeighbors(GridPoint current, HashMap<GridPoint, GridPoint> cameFrom) {
		ArrayList<GridPoint> neighbors = new ArrayList<GridPoint>();
		GenericTile gt = grid.getObjectAt(current.x, current.y);
		if (gt instanceof Road)
			return computeRoadNeighbors(current, (Road) grid.getObjectAt(current.x, current.y), cameFrom);
		if (gt instanceof VerticalCrosswalk)
			return computeVerticalCrosswalkNeighbors(current, (VerticalCrosswalk) grid.getObjectAt(current.x, current.y));
		if (gt instanceof HorizontalCrosswalk)
			return computeHorizontalCrosswalkNeighbors(current, (HorizontalCrosswalk) grid.getObjectAt(current.x, current.y));
		return neighbors;
	}
	
	private ArrayList<GridPoint> computeRoadNeighbors(GridPoint current, Road road, HashMap<GridPoint, GridPoint> cameFrom) {
		if (road.getMapRepr().equals("1_sq_nsew"))
			return computeCrossroadNeighbors(current, getSquareInfo(current, cameFrom));
		ArrayList<GridPoint> neighbors = new ArrayList<GridPoint>();
		for (GridPoint neigh : mapRoadNeighbor.get(road.getMapRepr())) {
			if (isInGrid(current.x + neigh.x, current.y + neigh.y))
				neighbors.add(mapCopy.get(current.y + neigh.y).get(current.x + neigh.x));
		}
		return neighbors;
	}
	
	private ArrayList<GridPoint> computeCrossroadNeighbors(GridPoint current, Tuple<Integer, Direction> currentInfo)
	{
		ArrayList<GridPoint> neighbors = new ArrayList<GridPoint>();
		if (currentInfo == null)
			return neighbors;
		for (Tuple<Integer, Direction> key : mapCrossroadNeighbor.keySet()) {
			if (key.getA().equals(currentInfo.getA()) && key.getB().equals(currentInfo.getB())) {
				for (GridPoint offset : mapCrossroadNeighbor.get(key)) {
					if (isInGrid(current.x + offset.x, current.y + offset.y))
						neighbors.add(mapCopy.get(current.y + offset.y).get(current.x + offset.x));
				}
				break;
			}
		}
		return neighbors;
	}
	
	private Tuple<Integer, Direction> getSquareInfo(GridPoint current, HashMap<GridPoint, GridPoint> cameFrom) {
		int i = 1;
		GridPoint gp_parent = current;
		for (; i <= 4; i++) {
			gp_parent = cameFrom.get(gp_parent);
			if (gp_parent == null)
				break;
			if (! (grid.getObjectAt(gp_parent.x, gp_parent.y) instanceof Road
					&& ((Road)grid.getObjectAt(gp_parent.x, gp_parent.y)).getMapRepr().equals("1_sq_nsew")))
				break;
		}
		
		if (gp_parent != null)
			return new Tuple<Integer, Direction>(i, computeDirection(cameFrom.get(current), current));
		
		return null;
	}
	
	private Direction computeDirection(GridPoint from, GridPoint to) {
		int x = to.x - from.x;
		int y = to.y - from.y;

		if (x == 0 && y == 1)
			return Direction.NORTH;
		if (x == 0 && y == -1)
			return Direction.SOUTH;
		if (x == 1 && y == 0)
			return Direction.EAST;
		if (x == -1 && y == 0)
			return Direction.WEST;

		return null;
	}

	private ArrayList<GridPoint> computeVerticalCrosswalkNeighbors(GridPoint current, VerticalCrosswalk crosswalk) {
		ArrayList<GridPoint> neighbors = null;
		neighbors = getNeighAtOffset(current, getNeighTile(current, -1, 0), 1, 0);
		if (neighbors != null)
			return neighbors;
		neighbors = getNeighAtOffset(current, getNeighTile(current, 1, 0), 1, 0);
		if (neighbors != null)
			return neighbors;
		return new ArrayList<GridPoint>();
	}
	
	private ArrayList<GridPoint> computeHorizontalCrosswalkNeighbors(GridPoint current, HorizontalCrosswalk crosswalk) {
		ArrayList<GridPoint> neighbors = null;
		neighbors = getNeighAtOffset(current, getNeighTile(current, 0, -1), 0, 1);
		if (neighbors != null)
			return neighbors;
		neighbors = getNeighAtOffset(current, getNeighTile(current, 0, 1), 0, 1);
		if (neighbors != null)
			return neighbors;
		return new ArrayList<GridPoint>();
	}

	private GenericTile getNeighTile(GridPoint gp, int x, int y) {
		return isInGrid(gp.x + x, gp.y + y) ? grid.getObjectAt(gp.x + x, gp.y + y) : null;
	}
	
	private ArrayList<GridPoint> getNeighAtOffset(GridPoint current, GenericTile neigh, int offsetX, int offsetY) {
		ArrayList<GridPoint> neighbors = new ArrayList<GridPoint>();;
		if (neigh != null && neigh instanceof Road && !((Road) neigh).getMapRepr().equals("1_sq_nsew")) {
			Road road = (Road) neigh;
			for (GridPoint gp : mapRoadNeighbor.get(road.getMapRepr()))
				if (Math.abs(gp.x) == offsetX && Math.abs(gp.y) == offsetY
					&& isInGrid(current.x + gp.x, current.y + gp.y))
				{
					neighbors.add(mapCopy.get(current.y + gp.y).get(current.x + gp.x));
					return neighbors;
				}
		}
		return null;
	}


	private boolean GotPedestrian(int x, int y)
	{
		Iterable<GenericTile> gtI = this.grid.getObjectsAt(x, y);
		for (GenericTile gt : gtI)
			if (gt instanceof Pedestrians)
				return true;
		return false;
	}

	@Override
	protected boolean CheckForTrafficLight(GridPoint gp)
	{
		Iterable<GenericTile> gtI = this.grid.getObjectsAt(gp.x, gp.y);
		for (GenericTile gt : gtI)
		{
			if (gt instanceof Crosswalk)
			{
				Crosswalk cw = (Crosswalk)gt;
				int order = cw.getOrder();
				TrafficLight tl = cw.getAssociatedTrafficLight();
				int x = this.grid.getLocation(cw).getX();
				int y = this.grid.getLocation(cw).getY();
				switch(tl.getDir())
				{
				case 'n':
					y--;
					break;
				case 's':
					y++;
					break;
				case 'w':
					x++;
					break;
				case 'e':
					x--;
					break;
				}
				if (tl != null)
				{
					LightState ls = tl.getLightState();
					if (order == 1)
						return ls.equals(LightState.GREEN);
					else
						return !GotPedestrian(x, y);
				}
			}
		}
		return true;
	}
	
	protected boolean CheckForCars(GridPoint gp)
	{
		Iterable<GenericTile> gtI = this.grid.getObjectsAt(gp.x, gp.y);
		for (GenericTile gt : gtI)
			if (gt instanceof Vehicle)
				return false;
		return true;
	}
}
