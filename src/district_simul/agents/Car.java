package district_simul.agents;

import district_simul.GenericTile;
import district_simul.utils.GridPoint;
import repast.simphony.context.Context;
import repast.simphony.space.grid.Grid;

public class Car extends Vehicle{

	public Car(Grid<GenericTile> gridCopy, Context<GenericTile> contextCopy, GridPoint startPath, GridPoint endPath, int uniqueId) {
		super(gridCopy, contextCopy, startPath, endPath, uniqueId);
		path = computePath();
		if (path.size() > 0)
			path.pop();
	}
	
	@Override
	public void SetNewPath(GridPoint start, GridPoint goal)
	{
		this.start = start;
		this.goal = goal;
		this.path = this.computePath();
		if (this.path.size() > 0)
			this.path.pop();
	}
}
