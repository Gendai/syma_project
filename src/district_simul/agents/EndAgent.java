package district_simul.agents;

import java.util.HashMap;

import district_simul.ContextCreator;
import district_simul.GenericTile;
import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.grid.Grid;
import repast.simphony.engine.environment.RunEnvironment;


public class EndAgent extends GenericTile
{
	private HashMap<Integer, Long> loc_cpy;
	
	public EndAgent(Grid<GenericTile> gridCopy, Context<GenericTile> contextCopy)
	{
		super(gridCopy, contextCopy);
	}
	
	@SuppressWarnings("unchecked")
	@ScheduledMethod(start = 1, interval = 1, priority = 2)
	public void CheckEnd()
	{
		this.loc_cpy = (HashMap<Integer, Long>)ContextCreator.agentExecTime.clone();
		
		boolean end = true;
		for (Long val : this.loc_cpy.values())
		{
			if (val == -1L)
				end = false;
		}
		
		if (end)
		{
			ContextCreator.agentExecTime.clear();
			ContextCreator.busLines.clear();
			Agent.mapCopy = null;
			RunEnvironment.getInstance().endRun();
		}
	}
	
	public long GetValues()
	{
		long tickCount = (long)RunEnvironment.getInstance().getCurrentSchedule().getTickCount() - 1;
		long sum = 0;
		for (long l : this.loc_cpy.values())
			if (l == tickCount)
				sum++;
		return sum;
	}
	
}
