package district_simul.agents;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Stack;

import district_simul.ContextCreator;
import district_simul.GenericTile;
import district_simul.utils.BusLine.BusStopLoc;
import district_simul.utils.GridPoint;
import repast.simphony.context.Context;
import repast.simphony.space.graph.Network;
import repast.simphony.space.grid.Grid;

public class Bus extends Vehicle{

	private int lineNum;
	private ArrayList<Object> busPath;
	private int pathIndex = 0;
	private ArrayList<BusStopLoc> busStops = new ArrayList<>();
	private HashMap<Integer, GridPoint> busStopsRoad = new HashMap<>();
	public boolean stoped = false;
	public int currStop = -1;
	private HashMap<Integer, ArrayList<OnBoardPassenger>> passengers = new HashMap<>();
	private ArrayList<Integer> busStopPathIndex = new ArrayList<>();
	private int freeSpots = 20;
	private boolean stopAt = false;
	private Network<GenericTile> net;
	
	public Bus(Grid<GenericTile> gridCopy, Context<GenericTile> contextCopy, GridPoint startPath, GridPoint endPath,
			   int nbLine, int uniqueId, int stopStart, Network<GenericTile> net) {
		super(gridCopy, contextCopy, startPath, endPath, uniqueId);
		lineNum = nbLine;
		this.busStops = ContextCreator.busLines.get(lineNum).getBusStops();
		this.net = net;
		computeBusPath();
		if (stopStart != 0)
			pathIndex = busStopPathIndex.get(stopStart);
	}
	
	private void computeBusPath()
	{
		Stack<GridPoint> res = new Stack<>();
		if (busStops.size() > 1)
		{
			for(int i = 0; i < busStops.size() - 1; ++i)
			{
				BusStopLoc bsl = busStops.get(i);
				GridPoint BusStopLocI = bsl.busStop.GetRoad();
				busStopsRoad.put(bsl.id, BusStopLocI);
				BusStopLoc bsl1 = busStops.get(i + 1);
				GridPoint BusStopLocI1 = bsl1.busStop.GetRoad();
				busStopsRoad.put(bsl1.id, BusStopLocI1);
				Stack<GridPoint> sgp = computePath(BusStopLocI.x, BusStopLocI.y,
												   BusStopLocI1.x, BusStopLocI1.y);
				sgp.addAll(res);
				res = sgp;
				busStopPathIndex.add(res.size());
			}
			BusStopLoc bsl = busStops.get(busStops.size() - 1);
			GridPoint BusStopLocI = bsl.busStop.GetRoad();
			busStopsRoad.put(bsl.id, BusStopLocI);
			BusStopLoc bsl1 = busStops.get(0);
			GridPoint BusStopLocI1 = bsl1.busStop.GetRoad();
			busStopsRoad.put(bsl1.id, BusStopLocI1);
			Stack<GridPoint> sgp = computePath(BusStopLocI.x, BusStopLocI.y,
											   BusStopLocI1.x, BusStopLocI1.y);
			sgp.addAll(res);
			res = sgp;
			busStopPathIndex.add(res.size());
		}
		path = res;
		busPath = new ArrayList<>(Arrays.asList(path.toArray()));
		Collections.reverse(busPath);
		Collections.reverse(busStopPathIndex);
		if (busPath.size() > 0)
			busPath.remove(0);
	}
	
	public int GetBusStop(GridPoint gp)
	{
		for (Entry<Integer, GridPoint> entry : busStopsRoad.entrySet())
		{
			if (gp.areEqual(entry.getValue()))
				return entry.getKey();
		}
		return -1;
	}

	@Override
	public void nextMove()
	{
		this.goingToMove = false;
		if (busPath.size() > 0)
		{
			if (pathIndex == busPath.size())
				pathIndex = 0;
			GridPoint toGo = (GridPoint)busPath.get(pathIndex);
			boolean carsB = CheckForCars(toGo);
			if (CheckForTrafficLight(toGo) && (carsB || stopAt))
			{
				this.grid.moveTo(this, toGo.x, toGo.y);
				pathIndex++;
				int busStopId = GetBusStop(toGo);
				this.goingToMove = true;
				if (busStopId != -1)
				{
					SpawnPassengers(busStopId);
					stopAt = true;
				}
				else
					stopAt = false;
			}
		}
		else
			this.context.remove(this);
	}
	
	private void SpawnPassengers(int id)
	{
		ArrayList<OnBoardPassenger> lsgp = passengers.get(id);
		if (lsgp != null)
		{
			for (OnBoardPassenger sgp : lsgp)
			{
				GridPoint start = sgp.gp.pop();
				GridPoint gpend = sgp.gp.firstElement();
				Pedestrians p = new Pedestrians(grid, context, sgp.gp, start, sgp.id);
				context.add(p);
				grid.moveTo(p, start.x, start.y);
				net.addEdge(p, this.grid.getObjectAt(gpend.x, gpend.y));
				freeSpots++;
			}
		}
		passengers.put(id, new ArrayList<>());
	}
	
	public boolean OnBoard(Pedestrians p, int busStopIndex)
	{
		if (freeSpots > 0)
		{
			Stack<GridPoint> passengerPath = p.getPath();
			ArrayList<OnBoardPassenger> lsgp = passengers.get(busStopIndex);
			if (lsgp == null)
			{
				ArrayList<OnBoardPassenger> arr = new ArrayList<>();
				arr.add(new OnBoardPassenger(passengerPath, p.uniqueId));
				passengers.put(busStopIndex, arr);
			}
			else
			{
				lsgp.add(new OnBoardPassenger(passengerPath, p.uniqueId));
				passengers.put(busStopIndex, lsgp);
			}
			freeSpots--;
			return true;
		}
		else
			return false;
	}
	
	public String NumberPassengers()
	{
		int sum = 0;
		for (ArrayList<OnBoardPassenger> lst : passengers.values())
			sum += lst.size();
		return "" + sum;
	}

}

class OnBoardPassenger
{
	public Stack<GridPoint> gp;
	public int id;
	
	public OnBoardPassenger(Stack<GridPoint> gp, int id)
	{
		this.gp = gp;
		this.id = id;
	}
}
