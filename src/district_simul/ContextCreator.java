package district_simul;

import java.io.FileNotFoundException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

import district_simul.agents.Bus;
import district_simul.agents.Car;
import district_simul.agents.EndAgent;
import district_simul.agents.Pedestrians;
import district_simul.agents.Vehicle;
import district_simul.tiles.Destination;
import district_simul.tiles.Road;
import district_simul.tiles.Sidewalk;
import district_simul.tiles.TrafficLight;
import district_simul.utils.BusLine;
import district_simul.utils.BusLine.BusStopLoc;
import district_simul.utils.GridPoint;
import district_simul.utils.MapReader;
import district_simul.utils.StaticTrafficLightsManager;
import district_simul.utils.TrafficLightsManager;
import district_simul.utils.WaitingLineTrafficLightsManager;
import repast.simphony.context.Context;
import repast.simphony.context.space.graph.NetworkBuilder;
import repast.simphony.context.space.grid.GridFactory;
import repast.simphony.context.space.grid.GridFactoryFinder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.space.graph.Network;
import repast.simphony.space.grid.Grid;

public class ContextCreator implements ContextBuilder<GenericTile> 
{
	public static final HashMap<Integer, BusLine> busLines = new HashMap<>();
	
	public static final HashMap<Integer, Long> agentExecTime = new HashMap<>();
	
	public int uniqueIdGen = 0;
	
	private int pedNum, vehNum, managerType;
	private ArrayList<GridPoint> sidewalks = new ArrayList<>();
	private ArrayList<GridPoint> roads = new ArrayList<>();
	public static int grid_width = 0;
	public static int grid_height = 0;
	
	public static TrafficLightsManager tlm;
	
	private void SetSideWalksAndRoads(Grid<GenericTile> grid, int width, int height)
	{
		for (int y = 0; y < height; ++y)
			for (int x = 0; x < width; ++x)
			{
				if (grid.getObjectAt(x, y) instanceof Sidewalk)
					sidewalks.add(new GridPoint(x, y));
				else if (grid.getObjectAt(x, y) instanceof Road)
					roads.add(new GridPoint(x, y));
			}
	}
	
	private GridPoint GetRoadDesti(Grid<GenericTile> grid, int width, int height, int x, int y) throws InvalidParameterException
	{
		if (x - 1 > 0 && grid.getObjectAt(x - 1, y) instanceof Road)
			return new GridPoint(x - 1, y);
		else if (x + 1 < width && grid.getObjectAt(x + 1, y) instanceof Road)
			return new GridPoint(x + 1, y);
		else if (y - 1 > 0 && grid.getObjectAt(x, y - 1) instanceof Road)
			return new GridPoint(x, y - 1);
		else if (y + 1 < height && grid.getObjectAt(x, y + 1) instanceof Road)
			return new GridPoint(x, y + 1);
		else
			throw new InvalidParameterException("This destination is not connected to any roads (x = "+x+", y = "+y+")");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Context<GenericTile> build(Context<GenericTile> context) 
	{
		context.setId("District_simul");
		uniqueIdGen = 0;
		ContextCreator.agentExecTime.clear();
		ContextCreator.busLines.clear();
		GridFactory gridFactory = GridFactoryFinder.createGridFactory(null);
		NetworkBuilder<GenericTile> netBuilder = new NetworkBuilder<GenericTile>("agent network", context, true);
		netBuilder.buildNetwork();
		Network<GenericTile> net = (Network<GenericTile>)context.getProjection("agent network");
		MapReader mr = null;
		pedNum = RunEnvironment.getInstance().getParameters().getInteger("numberOfPed");
		vehNum = RunEnvironment.getInstance().getParameters().getInteger("numberOfVeh");
		managerType = RunEnvironment.getInstance().getParameters().getInteger("managerType");
		
		try {
			mr = new MapReader(context, gridFactory, RunEnvironment.getInstance().getParameters().getString("mapLocation"));
			mr.readGrid();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		if (mr != null)
		{
			grid_width = mr.getGridWidth();
			grid_height = mr.getGridHeight();
			SetSideWalksAndRoads(mr.getGrid(), mr.getGridWidth(), mr.getGridHeight());
			
			switch(managerType)
			{
			case 1:
				tlm = StaticTrafficLightsManager.getInstance();
				break;
			case 2:
				tlm = WaitingLineTrafficLightsManager.getInstance();
				break;
			default:
				tlm = StaticTrafficLightsManager.getInstance();
				break;
			}
			
			tlm.Clear();
			
			for (TrafficLight tl : mr.getTrafficLightList())
				tlm.AddTrafficLight(mr.getGrid(), tl);
			
			if (mr.getDestinationList().size() > 0)
			{
				for (int i = 0; i < pedNum; ++i)
				{
					boolean foundPath = false;
					Pedestrians p = null;
					GridPoint sw = null;
					Destination dest = null;
					repast.simphony.space.grid.GridPoint gpd = null;
					for (int maxi = 0; maxi < 100 && !foundPath; ++maxi)
					{
						int sidewalkRand = ThreadLocalRandom.current().nextInt(0, sidewalks.size());
						int destRand = ThreadLocalRandom.current().nextInt(0, mr.getDestinationList().size());
						sw = sidewalks.get(sidewalkRand);
						dest = mr.getDestinationList().get(destRand);
						gpd = mr.getGrid().getLocation(dest);
						GridPoint spawnPoint = new GridPoint(sw.x, sw.y);				
						if (p == null)
							p = new Pedestrians(mr.getGrid(), context, spawnPoint, new GridPoint(gpd.getX(), gpd.getY()), uniqueIdGen);
						else
							p.SetNewPath(spawnPoint, new GridPoint(gpd.getX(), gpd.getY()));
						foundPath = p.hasPath();
					}
					if (sw !=null && dest != null && p != null)
					{
						context.add(p);
						mr.getGrid().moveTo(p, sw.x, sw.y);
						net.addEdge(p, dest);
						ContextCreator.agentExecTime.put(uniqueIdGen, -1L);
						uniqueIdGen++;
					}
				}
			
				for (int i = 0; i < vehNum; ++i)
				{
					boolean foundPath = false;
					Car c = null;
					GridPoint rw = null;
					Destination dst = null;
					GridPoint endP = null;
					repast.simphony.space.grid.GridPoint gpd = null;
					for (int maxi = 0; maxi < 100 && !foundPath; ++maxi)
					{
						rw = roads.get(ThreadLocalRandom.current().nextInt(0, roads.size()));
						GridPoint spawnPoint = new GridPoint(rw.x, rw.y);
						boolean taken = false;
						for (GenericTile gt : mr.getGrid().getObjectsAt(spawnPoint.x, spawnPoint.y))
							if (gt instanceof Vehicle)
							{
								taken = true;
								break;
							}
						if (taken)
							continue;
						dst =  mr.getDestinationList().get(ThreadLocalRandom.current().nextInt(0, mr.getDestinationList().size()));
						gpd = mr.getGrid().getLocation(dst);
						endP = GetRoadDesti(mr.getGrid(), mr.getGridWidth(), mr.getGridHeight(), gpd.getX(), gpd.getY());
						if (c == null)
							c = new Car(mr.getGrid(), context, spawnPoint, new GridPoint(endP.x, endP.y), uniqueIdGen);
						else
							c.SetNewPath(spawnPoint, new GridPoint(endP.x, endP.y));
						foundPath = c.hasPath();
					}
					
					if (rw !=null && dst != null && c != null)
					{
						context.add(c);
						mr.getGrid().moveTo(c, rw.x, rw.y);
						ContextCreator.agentExecTime.put(uniqueIdGen, -1L);
						net.addEdge(c, dst);
						uniqueIdGen++;
					}
				}
			}
			
			for (BusLine bl : busLines.values())
			{
				int size = bl.getBusStops().size();
				if (size >= 2)
				{
					Bus b = new Bus(mr.getGrid(), context, new GridPoint(0, 0), new GridPoint(0, 0), bl.getLineId(), uniqueIdGen++, 0, net);
					context.add(b);
					BusStopLoc bsl = bl.getBusStops().get(0);
					GridPoint broad = bsl.busStop.GetRoad();
					mr.getGrid().moveTo(b, broad.x, broad.y);
					if (size >= 4)
					{
						Bus b2 = new Bus(mr.getGrid(), context, new GridPoint(0, 0), new GridPoint(0, 0), bl.getLineId(), uniqueIdGen++, 1, net);
						context.add(b2);
						BusStopLoc bsl2 = bl.getBusStops().get(1);
						GridPoint broad2 = bsl2.busStop.GetRoad();
						mr.getGrid().moveTo(b2, broad2.x, broad2.y);
					}
				}
			}
			
			EndAgent ea = new EndAgent(mr.getGrid(), context);
			context.add(ea);
		}
		
		return context;
	}
}