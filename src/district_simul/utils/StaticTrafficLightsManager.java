package district_simul.utils;

import java.security.InvalidParameterException;

import district_simul.GenericTile;
import district_simul.tiles.TrafficLight;
import district_simul.tiles.TrafficLight.LightState;
import repast.simphony.space.grid.Grid;

public class StaticTrafficLightsManager extends TrafficLightsManager
{
	private StaticTrafficLightsManager()
    {}
     
    private static StaticTrafficLightsManager INSTANCE = new StaticTrafficLightsManager();
     
    public static StaticTrafficLightsManager getInstance()
    {
        return INSTANCE;
    }

	@Override
	public LightState GetLightState(Grid<GenericTile> grid, TrafficLight tf, long currTick) throws InvalidParameterException
	{
		Integer tick = trafficLightTick.get(tf);
		if (tick == null)
			throw new InvalidParameterException("No trafficLight instance in Manager : " + tf.getClass().getName());
		else
		{
			if (currTick % tick == 0)
				return (tf.getLightState().equals(LightState.GREEN) ? LightState.RED : LightState.GREEN);
			else
				return tf.getLightState();
		}
	}
}
