package district_simul.utils;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;

import district_simul.GenericTile;
import district_simul.agents.Vehicle;
import district_simul.tiles.TrafficLight;
import district_simul.tiles.TrafficLight.LightState;
import repast.simphony.space.grid.Grid;

public class WaitingLineTrafficLightsManager extends TrafficLightsManager
{
	
	private HashMap<Integer, Long> groupChanged = new HashMap<>();
	
	private WaitingLineTrafficLightsManager()
    {}
     
    private static WaitingLineTrafficLightsManager INSTANCE = new WaitingLineTrafficLightsManager();
     
    public static WaitingLineTrafficLightsManager getInstance()
    {
        return INSTANCE;
    }

    public int VehicleCount(Iterable<GenericTile> itGt)
    {
    	int count = 0;
    	for (GenericTile gt : itGt)
    		if (gt instanceof Vehicle)
    			count++;
    	return count;
    }
    
    public int WaitingLine(Grid<GenericTile> grid, TrafficLight tf)
    {
    	repast.simphony.space.grid.GridPoint gp = grid.getLocation(tf);
    	int vehicleCount = 0;
		if (tf.getDir() == 'e' || tf.getDir() == 'w')
		{
			int y = gp.getY();
			int x = gp.getX() + (tf.getDir() == 'e' ? 1 : -1);
			if (GoingSouth(grid, x, y))
				for (int yacc = -1; y + yacc > 0 && yacc > -4; yacc--)
					vehicleCount += VehicleCount(grid.getObjectsAt(x, y + yacc));
			else
				for (int yacc = 1; y + yacc < grid.getDimensions().getHeight() && yacc < 4; yacc++)
					vehicleCount += VehicleCount(grid.getObjectsAt(x, y + yacc));
		}
		else if (tf.getDir() == 's' || tf.getDir() == 'n')
		{
			int y = gp.getY() + (tf.getDir() == 's' ? -1 : 1);
			int x = gp.getX();
			if (GoingEast(grid, x, y))
				for (int xacc = -1; x + xacc > 0 && xacc > -4; --xacc)
					vehicleCount += VehicleCount(grid.getObjectsAt(x + xacc, y));
			else
				for (int xacc = 1; x + xacc < grid.getDimensions().getWidth() && xacc < 4; ++xacc)
					vehicleCount += VehicleCount(grid.getObjectsAt(x + xacc, y));
		}
		
		return vehicleCount;
    }
    
	@Override
	public LightState GetLightState(Grid<GenericTile> grid, TrafficLight tf, long currTick)
	{
		int groupIndex = trafficLightMap.get(tf);
		ArrayList<TrafficLight> tfList = groupListTf.get(groupIndex);
		int count = tfList.size();
		int vehCount = WaitingLine(grid, tf);
		Integer groupTick = trafficLightsGroupTick.get(groupIndex);
		Integer newVal = trafficLightTick.get(tf);
		
		if (newVal == null)
			throw new InvalidParameterException("No trafficLight instance in Manager : " + tf.getClass().getName());
		
		Long tickGroupChange = groupChanged.get(groupIndex);
		if ((tickGroupChange == null || tickGroupChange != currTick)
			 && vehCount > 3)
		{
			if (count > 3)
				newVal = (groupTick - 10 >= 40 ? groupTick - 10 : 40);
			else
				newVal = (groupTick + 10 <= 80 ? groupTick + 10 : 80);
			for (TrafficLight tl : tfList)
				trafficLightTick.put(tl, newVal);
			trafficLightsGroupTick.put(groupIndex, newVal);
			groupChanged.put(groupIndex, currTick);
		}
		
		if (currTick % newVal == 0)
			return (tf.getLightState().equals(LightState.GREEN) ? LightState.RED : LightState.GREEN);
		else
			return tf.getLightState();
	}
}
