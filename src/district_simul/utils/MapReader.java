package district_simul.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.ArrayList;

import district_simul.ContextCreator;
import district_simul.GenericTile;
import district_simul.tiles.BusStop;
import district_simul.tiles.Crosswalk;
import district_simul.tiles.Destination;
import district_simul.tiles.HorizontalCrosswalk;
import district_simul.tiles.Road;
import district_simul.tiles.Sidewalk;
import district_simul.tiles.TrafficLight;
import district_simul.tiles.VerticalCrosswalk;
import district_simul.tiles.Road.Direction;
import repast.simphony.context.Context;
import repast.simphony.context.space.grid.GridFactory;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridBuilderParameters;
import repast.simphony.space.grid.SimpleGridAdder;
import repast.simphony.space.grid.WrapAroundBorders;

public class MapReader {

	private Context<GenericTile> context;
	private Grid<GenericTile> grid;
	private GridFactory gridFactory;
	private FileReader fileReader;
	
	private ArrayList<TrafficLight> trafficLightList = new ArrayList<>();
	private ArrayList<Destination> destinationList = new ArrayList<>();
	private int gridWidth;
	private int gridHeight;
	
	public MapReader(Context<GenericTile> context, GridFactory gridFactory, String fname) throws FileNotFoundException
	{
		this.context = context;
		this.gridFactory = gridFactory;
		this.fileReader = new FileReader(fname);
	}
	
	public ArrayList<TrafficLight> getTrafficLightList()
	{
		return this.trafficLightList;
	}
	
	public ArrayList<Destination> getDestinationList()
	{
		return this.destinationList;
	}
	
	public Grid<GenericTile> getGrid() {
		return grid;
	}
	
	public void readGrid()
	{
		
		try (BufferedReader br = new BufferedReader(this.fileReader))
		{
			String currentLine;
			boolean sizeDef = false;
			int xPos = 0;
			int yPos = 0;

			while ((currentLine = br.readLine()) != null)
			{
				if (!sizeDef)
				{
					//width;height
					String[] sizes = currentLine.split(";");
					int width = Integer.parseInt(sizes[0]);
					int height = Integer.parseInt(sizes[1]);
					
					this.grid = gridFactory.createGrid("grid", context, new GridBuilderParameters<GenericTile>(
							new WrapAroundBorders(), new SimpleGridAdder<GenericTile>(), true, width, height));
					sizeDef = true;
					this.gridWidth = width;
					this.gridHeight = height;
					yPos = height - 1;
				}
				else
				{
					String[] mapTypeIds = currentLine.split(";");
					for (String mapTypeId : mapTypeIds)
					{
						if (mapTypeId.equals("0"))
						{
							xPos++;
							continue;
						}
						GenericTile gt = tileInstance(mapTypeId);
						if (gt instanceof BusStop)
						{
							BusStop bs = (BusStop)gt;
							if (ContextCreator.busLines.containsKey(bs.getLineId()))
								ContextCreator.busLines.get(bs.getLineId()).addBusStop(bs, new GridPoint(xPos, yPos));
							else
							{
								BusLine bl = new BusLine(bs.getLineId());
								ContextCreator.busLines.put(bs.getLineId(), bl);
								int id = bl.addBusStop(bs, new GridPoint(xPos, yPos));
								bs.setStopId(id);
							}
						}
						else if (gt instanceof TrafficLight)
							trafficLightList.add((TrafficLight)gt);
						else if (gt instanceof Destination)
							destinationList.add((Destination)gt);
							
						this.context.add(gt);
						this.grid.moveTo(gt, xPos, yPos);
						xPos++;
					}
					xPos = 0;
					yPos--;
				}
					
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		ConnectTrafficLightsCrosswalks();
	}
	
	private ArrayList<Direction> DirectionFromMapHelper(String directions) throws InvalidParameterException
	{
		ArrayList<Direction> directionsList = new ArrayList<>();
		for (int index = 0; index < directions.length(); ++index)
		{
			char dir = directions.charAt(index);
			switch (dir)
			{
			case 'n':
				directionsList.add(Direction.NORTH);
				break;
			case 's':
				directionsList.add(Direction.SOUTH);
				break;
			case 'w':
				directionsList.add(Direction.WEST);
				break;
			case 'e':
				directionsList.add(Direction.EAST);
				break;
			default:
				throw new InvalidParameterException("No match for directions from directions = " + directions);
			}
		}
		return directionsList;
	}
	
	private GenericTile tileInstance(String mapTypeId) throws InvalidParameterException
	{
		switch(mapTypeId.charAt(0))
		{
		case '1':
			String[] roadInfos = mapTypeId.split("_");
			return new Road(this.grid, this.context, DirectionFromMapHelper(roadInfos[2]), mapTypeId);
		case '2':
			return new Sidewalk(this.grid, this.context);
		case '3':
			String[] tfInfos = mapTypeId.split("_");
			return new TrafficLight(this.grid, this.context, tfInfos[1].charAt(0));
		case '4':
			if (mapTypeId.charAt(2) == 'v')
				return new VerticalCrosswalk(this.grid, this.context);
			else if (mapTypeId.charAt(2) == 'h')
				return new HorizontalCrosswalk(this.grid, this.context);
			else
				throw new InvalidParameterException("No match for corsswalk orientation (" + mapTypeId.charAt(2) + ")");
		case '5':
			String[] infos = mapTypeId.split("_");
			return new BusStop(this.grid, this.context, Integer.parseInt(infos[2]), infos[1].charAt(0));
		case '6':
			return new Destination(this.grid, this.context); 
		default:
			throw new InvalidParameterException("No matchin TileType for this mapTypeId = " + mapTypeId);
		}
	}
	
	private void ConnectCrosswalks(int x, int y, int xacc, int yacc, int acc, TrafficLight tl)
	{
		Crosswalk cw = (Crosswalk)this.grid.getObjectAt(x + xacc, y + yacc);
		int order = cw.getOrder();
		if(order  == 0 || order > acc)
		{
			if (order != 0)
				cw.getAssociatedTrafficLight().RemoveCrosswalk(cw);
			cw.setAssociatedTrafficLight(tl, acc);
			tl.AddCrosswalk(cw);
		}
	}
	
	private void ConnectTrafficLightsCrosswalks()
	{
		for (TrafficLight tf : trafficLightList)
		{
			repast.simphony.space.grid.GridPoint gp = this.grid.getLocation(tf);
			switch(tf.getDir())
			{
			case 'n':
				for (int acc = 1; gp.getY() + acc < this.grid.getDimensions().getHeight() 
					 && this.grid.getObjectAt(gp.getX(), gp.getY() + acc) instanceof Crosswalk; ++acc)
					ConnectCrosswalks(gp.getX(), gp.getY(), 0, acc, acc, tf);
				break;
			case 's':
				for (int acc = -1; gp.getY() + acc > 0
						&& this.grid.getObjectAt(gp.getX(), gp.getY() + acc) instanceof Crosswalk; --acc)
					ConnectCrosswalks(gp.getX(), gp.getY(), 0, acc, -acc, tf);
				break;
			case 'w':
				for (int acc = -1; gp.getX() + acc > 0 
						&& this.grid.getObjectAt(gp.getX() + acc, gp.getY()) instanceof Crosswalk; --acc)
					ConnectCrosswalks(gp.getX(), gp.getY(), acc, 0, -acc, tf);
				break;
			case 'e':
				for (int acc = 1; gp.getX() + acc < this.grid.getDimensions().getWidth() 
						&& this.grid.getObjectAt(gp.getX() + acc, gp.getY()) instanceof Crosswalk; ++acc)
					ConnectCrosswalks(gp.getX(), gp.getY(), acc, 0, acc, tf);
				break;
			default:
				System.out.println("[ERROR] Wrong traffic light direction : "+tf.getDir());
				break;
			}
		}
	}

	public int getGridWidth() {
		return gridWidth;
	}

	public int getGridHeight() {
		return gridHeight;
	}
}
