package district_simul.utils;

public class GridPoint {
	
	public int x, y;
	private boolean waitForBus = false;
	
	public GridPoint(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public GridPoint(int x, int y, boolean waitForBus)
	{
		this.x = x;
		this.y = y;
		this.waitForBus = waitForBus;
	}
	
	public boolean areEqual(GridPoint rightGridPoint) {
		return this.x == rightGridPoint.x && this.y == rightGridPoint.y;
	}
	
	public GridPoint Copy()
	{
		return new GridPoint(this.x, this.y);
	}
	
	public void SetFrom(GridPoint gp)
	{
		this.x = gp.x;
		this.y = gp.y;
	}
	
	public void setWaitForBus(boolean b)
	{
		waitForBus = b;
	}
	
	public boolean getWaitForBus()
	{
		return waitForBus;
	}
}
