package district_simul.utils;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

import district_simul.GenericTile;
import district_simul.tiles.Crosswalk;
import district_simul.tiles.HorizontalCrosswalk;
import district_simul.tiles.Road;
import district_simul.tiles.Road.Direction;
import district_simul.tiles.TrafficLight;
import district_simul.tiles.TrafficLight.LightState;
import district_simul.tiles.VerticalCrosswalk;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;

public abstract class TrafficLightsManager
{
	protected HashMap<Integer, Integer> trafficLightsGroupTick = new HashMap<>();
	protected HashMap<TrafficLight, Integer> trafficLightMap = new HashMap<>();
	protected HashMap<TrafficLight, Integer> trafficLightTick = new HashMap<>();
	protected HashMap<Integer, ArrayList<TrafficLight>> groupListTf = new HashMap<>();
	private Integer groupIndex = 0;
	
	
	public void PrintInfos(Grid<GenericTile> grid)
	{
		for (TrafficLight tf : trafficLightMap.keySet())
		{
			GridPoint gp = grid.getLocation(tf);
			System.out.println("Tf at ("+gp.getX()+", "+gp.getY()+"), with group = "+trafficLightMap.get(tf));
		}
		
		for (Integer group : groupListTf.keySet())
		{
			System.out.println("From group : "+group);
			ArrayList<TrafficLight> taf = groupListTf.get(group);
			for (TrafficLight tf : taf)
			{
				GridPoint gp = grid.getLocation(tf);
				int tick = trafficLightTick.get(tf);
				System.out.println("Tf at ("+gp.getX()+", "+gp.getY()+"), tick = "+tick);
			}
		}
	}
	
	public void Clear()
	{
		this.trafficLightsGroupTick.clear();
		this.trafficLightMap.clear();
		this.trafficLightTick.clear();
		this.groupListTf.clear();
		this.groupIndex = 0;
	}
	
	private void AddtoMaps(TrafficLight tf, TrafficLight tf2, int sign)
	{
		Integer group2 = trafficLightMap.get(tf2);
		Integer group1 = trafficLightMap.get(tf);
		
		if (group2 != null && group1 != null && group1 != group2)
		{
			trafficLightMap.put(tf2, group1);
			ArrayList<TrafficLight> arr = groupListTf.get(group1);
			if (arr == null)
				arr = new ArrayList<>();
			ArrayList<TrafficLight> arr2 = groupListTf.get(group2);
			arr.add(tf2);
			arr.addAll(arr2);
			
			groupListTf.put(group2, new ArrayList<>());
			groupListTf.put(group1, arr);
			int gpTick = trafficLightTick.get(tf);
			trafficLightTick.put(tf2, gpTick);
			for (TrafficLight tfar2 : arr2)
			{
				trafficLightMap.put(tfar2, group1);
				trafficLightTick.put(tfar2, gpTick);
			}
			
			LightState ls = tf.getLightState();
			LightState ls2 = ls;
			if (sign < 0)
				ls2 = (ls.equals(LightState.GREEN) ? LightState.RED : LightState.GREEN);
			tf2.setLightState(ls2);
		}
		
		if (group2 == null && group1 == null)
		{
			trafficLightMap.put(tf2, groupIndex);
			trafficLightMap.put(tf, groupIndex);
			ArrayList<TrafficLight> arr = groupListTf.get(groupIndex);
			if (arr == null)
				arr = new ArrayList<>();
			arr.add(tf);
			arr.add(tf2);
			groupListTf.put(groupIndex, arr);
			int rndTick = ThreadLocalRandom.current().nextInt(40, 70);
			trafficLightsGroupTick.put(groupIndex, rndTick);
			trafficLightTick.put(tf2, rndTick);
			trafficLightTick.put(tf, rndTick);
			tf.setLightState(LightState.GREEN);
			if (sign < 0)
				tf2.setLightState(LightState.RED);
			else
				tf2.setLightState(LightState.GREEN);
			groupIndex++;
		}
		else if (group1 == null && group2 != null)
		{
			trafficLightMap.put(tf, group2);
			ArrayList<TrafficLight> arr = groupListTf.get(group2);
			if (arr == null)
				arr = new ArrayList<>();
			arr.add(tf);
			groupListTf.put(group2, arr);
			int gpTick = trafficLightTick.get(tf2);
			trafficLightTick.put(tf, gpTick);
			LightState ls2 = tf2.getLightState();
			LightState ls = ls2;
			if (sign < 0)
				ls = (ls2.equals(LightState.GREEN) ? LightState.RED : LightState.GREEN);
			tf.setLightState(ls);
		}
		else if (group1 != null && group2 == null)
		{
			trafficLightMap.put(tf2, group1);
			ArrayList<TrafficLight> arr = groupListTf.get(group1);
			if (arr == null)
				arr = new ArrayList<>();
			arr.add(tf2);
			groupListTf.put(group1, arr);
			int gpTick = trafficLightTick.get(tf);
			trafficLightTick.put(tf2, gpTick);
			LightState ls = tf.getLightState();
			LightState ls2 = ls;
			if (sign < 0)
				ls2 = (ls.equals(LightState.GREEN) ? LightState.RED : LightState.GREEN);
			tf2.setLightState(ls2);
		}
	}
	
	private boolean AddNearHorizontal(Grid<GenericTile> grid, TrafficLight tf, int x, int y, int xacc, int yacc, int sign)
	{
		if(!(grid.getObjectAt(x + xacc, y + yacc) instanceof Road)
			&& !(grid.getObjectAt(x + xacc, y + yacc) instanceof Crosswalk))
			return true;
		if (grid.getObjectAt(x + xacc, y + yacc) instanceof HorizontalCrosswalk)
		{
			HorizontalCrosswalk hc = (HorizontalCrosswalk)grid.getObjectAt(x + xacc, y + yacc);
			AddtoMaps(tf, hc.getAssociatedTrafficLight(), sign);
		}
		return false;
	}
	
	private boolean AddNearVertical(Grid<GenericTile> grid, TrafficLight tf, int x, int y, int xacc, int yacc, int sign)
	{
		if(!(grid.getObjectAt(x + xacc, y + yacc) instanceof Road)
			&& !(grid.getObjectAt(x + xacc, y + yacc) instanceof Crosswalk))
			return true;
		if (grid.getObjectAt(x + xacc, y + yacc) instanceof VerticalCrosswalk)
		{
			VerticalCrosswalk vc = (VerticalCrosswalk)grid.getObjectAt(x + xacc, y + yacc);
			AddtoMaps(tf, vc.getAssociatedTrafficLight(), sign);
		}
		return false;
	}
	
	protected boolean GoingSouth(Grid<GenericTile> grid, int x, int y)
	{
		Road r = null;
		if (y + 1 < grid.getDimensions().getHeight() && grid.getObjectAt(x, y + 1) instanceof Road)
			r = (Road)grid.getObjectAt(x, y + 1);
		else if (y - 1 > 0  && grid.getObjectAt(x, y - 1) instanceof Road)
			r = (Road)grid.getObjectAt(x, y - 1);
		if (r.getRoadDirections().contains(Direction.SOUTH))
			return true;
		else if (r.getRoadDirections().contains(Direction.NORTH))
			return false;
		return true;
	}
	
	protected boolean GoingEast(Grid<GenericTile> grid, int x, int y)
	{
		Road r = null;
		if (x + 1 < grid.getDimensions().getWidth() && grid.getObjectAt(x + 1, y) instanceof Road)
			r = (Road)grid.getObjectAt(x + 1, y);
		else if (x - 1 > 0  && grid.getObjectAt(x - 1, y) instanceof Road)
			r = (Road)grid.getObjectAt(x - 1, y);
		if (r.getRoadDirections().contains(Direction.EAST))
			return true;
		else if (r.getRoadDirections().contains(Direction.WEST))
			return false;
		return true;
	}
	
	public void AddTrafficLight(Grid<GenericTile> grid, TrafficLight tf)
	{
		repast.simphony.space.grid.GridPoint gp = grid.getLocation(tf);
		if (tf.getDir() == 'e' || tf.getDir() == 'w')
		{
			int y = gp.getY();
			int x = gp.getX() + (tf.getDir() == 'e' ? 1 : -1);
			int yacc = 1;
			
			if (tf.getDir() == 'e' && grid.getObjectAt(x + 1, y) instanceof Crosswalk)
			{
				Crosswalk cw = (Crosswalk)grid.getObjectAt(x + 1, y);
				if (cw.getAssociatedTrafficLight() != tf)
					AddtoMaps(tf, cw.getAssociatedTrafficLight(), 1);
			}
			if (tf.getDir() == 'w' && grid.getObjectAt(x - 1, y) instanceof Crosswalk)
			{
				Crosswalk cw = (Crosswalk)grid.getObjectAt(x - 1, y);
				if (cw.getAssociatedTrafficLight() != tf)
					AddtoMaps(tf, cw.getAssociatedTrafficLight(), 1);
			}
			
			if (GoingSouth(grid, x, y))
			{
				for (yacc = -1; y + yacc > 0 && yacc > -5; yacc--)
				{
					if (AddNearHorizontal(grid, tf, x, y, 0, yacc, 1))
						continue;
					for (int xacc = -1; x + xacc > 0 && xacc > -5; xacc--)
						if (AddNearVertical(grid, tf, x, y, xacc, yacc, -1))
							continue;
				}
			}
			else
			{
				for (yacc = 1; y + yacc < grid.getDimensions().getHeight() && yacc < 5; yacc++)
				{
					if (AddNearHorizontal(grid, tf, x, y, 0, yacc, 1))
						continue;
					for (int xacc = 1; x + xacc < grid.getDimensions().getWidth() && xacc < 5; xacc++)
						if (AddNearVertical(grid, tf, x, y, xacc, yacc, -1))
							continue;
				}
			}
		}
		else if (tf.getDir() == 's' || tf.getDir() == 'n')
		{
			int y = gp.getY() + (tf.getDir() == 's' ? -1 : 1);
			int x = gp.getX();
			int xacc = 1;
			
			if (tf.getDir() == 's' && grid.getObjectAt(x, y - 1) instanceof Crosswalk)
			{
				Crosswalk cw = (Crosswalk)grid.getObjectAt(x, y - 1);
				if (cw.getAssociatedTrafficLight() != tf)
					AddtoMaps(tf, cw.getAssociatedTrafficLight(), 1);
			}
			if (tf.getDir() == 'n' && grid.getObjectAt(x, y + 1) instanceof Crosswalk)
			{
				Crosswalk cw = (Crosswalk)grid.getObjectAt(x, y + 1);
				if (cw.getAssociatedTrafficLight() != tf)
					AddtoMaps(tf, cw.getAssociatedTrafficLight(), 1);
			}
			if (GoingEast(grid, x, y))
			{
				for (xacc = -1; x + xacc > 0 && xacc > -5; --xacc)
				{
					if (AddNearVertical(grid, tf, x, y, xacc, 0, 1))
						continue;
					for (int yacc = 1; y + yacc < grid.getDimensions().getHeight() && yacc < 5; ++yacc)
						if (AddNearHorizontal(grid, tf, x, y, xacc, yacc, -1));
							continue;
				}
			}
			else
			{
				for (xacc = 1; x + xacc < grid.getDimensions().getWidth() && xacc < 5; ++xacc)
				{
					if (AddNearVertical(grid, tf, x, y, xacc, 0, 1))
						continue;
					for (int yacc = -1; y + yacc > 0 && yacc > -5; --yacc)
						if (AddNearHorizontal(grid, tf, x, y, xacc, yacc, -1));
							continue;
				}
			}
		}
		
		if (trafficLightTick.get(tf) == null)
		{
			trafficLightMap.put(tf, groupIndex);
			int rndTick = ThreadLocalRandom.current().nextInt(40, 70);
			trafficLightsGroupTick.put(groupIndex, rndTick);
			trafficLightTick.put(tf, rndTick);
			ArrayList<TrafficLight> arr = new ArrayList<>();
			arr.add(tf);
			groupListTf.put(groupIndex, arr);
			groupIndex++;
		}
	}
	
	public abstract LightState GetLightState(Grid<GenericTile> grid, TrafficLight tf, long currTick);
	
	public boolean TimeToCross(TrafficLight tf, long currTick)
	{
		Integer tick = trafficLightTick.get(tf);
		if (tick == null)
			throw new InvalidParameterException("No trafficLight instance in Manager : " + tf.getClass().getName());
		else
		{
			int crosswalkLength = tf.CrosswalkSize();
			return crosswalkLength <= (tick - currTick);
		}
	}
}
