package district_simul.utils;

import java.util.ArrayList;

import district_simul.tiles.BusStop;

public class BusLine {
	
	public class BusStopLoc
	{
		public BusStop busStop;
		public GridPoint gridPoint;
		public int id;
		public BusStopLoc(BusStop busStop, GridPoint gridPoint, int id) {
			this.busStop = busStop;
			this.gridPoint = gridPoint;
			this.id = id;
		}
	}

	private int lineId;
	private ArrayList<BusStopLoc> busStops;
	private int currId = 0;
	
	public BusLine(int lineId)
	{
		this.lineId = lineId;
		this.busStops = new ArrayList<>();
	}
	
	public int addBusStop(BusStop busStop, GridPoint gridPoint)
	{
		this.busStops.add(new BusStopLoc(busStop, gridPoint, currId));
		int idTmp = currId;
		currId++;
		return idTmp;
	}
	
	public int getLineId()
	{
		return this.lineId;
	}
	
	public ArrayList<BusStopLoc> getBusStops()
	{
		return this.busStops;
	}
}
