package district_simul.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import district_simul.tiles.Road;
import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;
import repast.simphony.visualizationOGL2D.ImageSpatialSource;
import saf.v3d.scene.VSpatial;

public class RoadStyle extends DefaultStyleOGL2D{
	
	private Map<String, String> mapIconType = new HashMap<String, String>();
	{
		mapIconType.put("1_dl_sw", ".\\icons\\road-icon_dl.png");     
		mapIconType.put("1_dr_se", ".\\icons\\road-icon_dr.png");
		mapIconType.put("1_h_e", ".\\icons\\road-icon_hori_east.png");
		mapIconType.put("1_h_w", ".\\icons\\road-icon_hori_west.png");
		mapIconType.put("1_ld_ws", ".\\icons\\road-icon_ld.png");     
		mapIconType.put("1_lf_wn", ".\\icons\\road-icon_lf.png");     
		mapIconType.put("1_rd_es", ".\\icons\\road-icon_rd.png");     
		mapIconType.put("1_ru_en", ".\\icons\\road-icon_ru.png"); 
		mapIconType.put("1_td_swe", ".\\icons\\road-icon_trid1.png");   
		mapIconType.put("1_td_ws", ".\\icons\\road-icon_trid2.png");  
		mapIconType.put("1_td_es", ".\\icons\\road-icon_trid3.png");  
		mapIconType.put("1_td_sw", ".\\icons\\road-icon_trid4.png");  
		mapIconType.put("1_td_se", ".\\icons\\road-icon_trid5.png"); 
		mapIconType.put("1_tl_wns", ".\\icons\\road-icon_tril1.png");   
		mapIconType.put("1_tl_sw", ".\\icons\\road-icon_tril2.png");  
		mapIconType.put("1_tl_nw", ".\\icons\\road-icon_tril3.png");  
		mapIconType.put("1_tl_wn", ".\\icons\\road-icon_tril4.png");  
		mapIconType.put("1_tl_ws", ".\\icons\\road-icon_tril5.png");  
		mapIconType.put("1_tr_ne", ".\\icons\\road-icon_trir1.png"); 
		mapIconType.put("1_tr_wns", ".\\icons\\road-icon_trir2.png");   
		mapIconType.put("1_tr_se", ".\\icons\\road-icon_trir3.png");  
		mapIconType.put("1_tr_ws", ".\\icons\\road-icon_trir4.png");  
		mapIconType.put("1_tr_wn", ".\\icons\\road-icon_trir5.png");  
	    mapIconType.put("1_tu_new", ".\\icons\\road-icon_triu1.png");   
		mapIconType.put("1_tu_wn", ".\\icons\\road-icon_triu2.png");  
		mapIconType.put("1_tu_en", ".\\icons\\road-icon_triu3.png");  
		mapIconType.put("1_tu_nw", ".\\icons\\road-icon_triu4.png");  
		mapIconType.put("1_tu_ne", ".\\icons\\road-icon_triu5.png");    
		mapIconType.put("1_ul_nw", ".\\icons\\road-icon_ul.png");     
		mapIconType.put("1_ur_ne", ".\\icons\\road-icon_ur.png");
		mapIconType.put("1_v_n", ".\\icons\\road-icon_ver_north.png");
		mapIconType.put("1_v_s", ".\\icons\\road-icon_ver_south.png");
		mapIconType.put("1_sq_nsew", ".\\icons\\road-icon_sq.png");
	}
	
		@Override
		public VSpatial getVSpatial(Object agent, VSpatial spatial) {
			if (agent instanceof Road)
			{
				Road r = (Road)agent;
				String path = mapIconType.get(r.getMapRepr());
				Map<String, String> props = new HashMap<String, String>();
				ImageSpatialSource imgSource;
				try {
					imgSource = new ImageSpatialSource(r.getMapRepr(), path);
					imgSource.registerSource(shapeFactory, props);
				} catch (IOException e) {
					e.printStackTrace();
				}
				return shapeFactory.getNamedSpatial(r.getMapRepr());
			}
			return null;
		}
}
