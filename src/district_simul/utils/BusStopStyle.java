package district_simul.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import district_simul.tiles.BusStop;
import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;
import repast.simphony.visualizationOGL2D.ImageSpatialSource;
import saf.v3d.scene.VSpatial;

public class BusStopStyle extends DefaultStyleOGL2D{

	@Override
	public VSpatial getVSpatial(Object agent, VSpatial spatial) {
		if (agent instanceof BusStop)
		{
			BusStop bs = (BusStop)agent;
			String path;
			switch(bs.getDir())
			{
			case 'n':
				path = ".\\icons\\bus_stop_ico_n.png";
				break;
			case 's':
				path = ".\\icons\\bus_stop_ico_s.png";
				break;
			case 'e':
				path = ".\\icons\\bus_stop_ico_e.png";
				break;
			case 'w':
				path = ".\\icons\\bus_stop_ico_w.png";
				break;
			default:
				path = "";
				break;
			}
			Map<String, String> props = new HashMap<String, String>();
			ImageSpatialSource imgSource;
			try {
				imgSource = new ImageSpatialSource("busStop" + bs.getDir(), path);
				imgSource.registerSource(shapeFactory, props);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return shapeFactory.getNamedSpatial("busStop"+bs.getDir());
		}
		return null;
	}
	
	@Override
	public String getLabel(Object agent)
	{
		BusStop bs = (BusStop)agent;
		return ""+bs.getLineId();
	}
	
	@Override
	public saf.v3d.scene.Position getLabelPosition(Object agent)
	{
		return saf.v3d.scene.Position.CENTER;
	}
	
	@Override
	public java.awt.Font getLabelFont(Object agent)
	{
		return new java.awt.Font("Arial", java.awt.Font.BOLD, 16);
	}
	
	@Override
	public java.awt.Color getLabelColor(Object agent)
	{
		return java.awt.Color.RED;
	}
	
	@Override
	public float getLabelYOffset(Object agent)
	{
		return -5.5f;
	}
	
	@Override
	public float getLabelXOffset(Object agent)
	{
		return -7.0f;
	}
}
