package district_simul.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import district_simul.tiles.TrafficLight;
import district_simul.tiles.TrafficLight.LightState;
import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;
import repast.simphony.visualizationOGL2D.ImageSpatialSource;
import saf.v3d.scene.VSpatial;

public class TrafficLightStyle extends DefaultStyleOGL2D{
	
	@Override
	public VSpatial getVSpatial(Object agent, VSpatial spatial) {
		if (agent instanceof TrafficLight)
		{
			Map<String, String> props = new HashMap<String, String>();
			TrafficLight tf = (TrafficLight)agent;
			String path;
			boolean isGreen = tf.getLightState().equals(LightState.GREEN);
			switch(tf.getDir())
			{
			case 'n':
				if (isGreen)
					path = ".\\icons\\traffic_lights_green_n.png";
				else
					path = ".\\icons\\traffic_lights_red_n.png";
				break;
			case 's':
				if (isGreen)
					path = ".\\icons\\traffic_lights_green_s.png";
				else
					path = ".\\icons\\traffic_lights_red_s.png";
				break;
			case 'e':
				if (isGreen)
					path = ".\\icons\\traffic_lights_green_e.png";
				else
					path = ".\\icons\\traffic_lights_red_e.png";
				break;
			case 'w':
				if (isGreen)
					path = ".\\icons\\traffic_lights_green_w.png";
				else
					path = ".\\icons\\traffic_lights_red_w.png";
				break;
			default:
				path = "";
				break;
			}
			ImageSpatialSource imgSource;
			String name = "tf" + tf.getDir() + (isGreen ? "g" : "r");
			try {
				imgSource = new ImageSpatialSource(name, path);
				imgSource.registerSource(shapeFactory, props);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return shapeFactory.getNamedSpatial(name);
		}
		return null;
	}

}
