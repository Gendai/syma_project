package district_simul.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import district_simul.agents.Bus;
import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;
import repast.simphony.visualizationOGL2D.ImageSpatialSource;
import saf.v3d.scene.VSpatial;

public class BusStyle extends DefaultStyleOGL2D {
	
	
	@Override
	public VSpatial getVSpatial(Object agent, VSpatial spatial) {
		if (agent instanceof Bus)
		{
			Map<String, String> props = new HashMap<String, String>();
			ImageSpatialSource imgSource;
			try {
				imgSource = new ImageSpatialSource("bus", ".\\icons\\bus-side-view.png");
				imgSource.registerSource(shapeFactory, props);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return shapeFactory.getNamedSpatial("bus");
		}
		return null;
	}
	
	@Override
	public String getLabel(Object agent)
	{
		Bus b = (Bus)agent;
		return ""+b.NumberPassengers();
	}
	
	@Override
	public saf.v3d.scene.Position getLabelPosition(Object agent)
	{
		return saf.v3d.scene.Position.CENTER;
	}
	
	@Override
	public java.awt.Font getLabelFont(Object agent)
	{
		return new java.awt.Font("Arial", java.awt.Font.BOLD, 16);
	}
	
	@Override
	public java.awt.Color getLabelColor(Object agent)
	{
		return java.awt.Color.RED;
	}
	
	@Override
	public float getLabelYOffset(Object agent)
	{
		return -5.5f;
	}
	
	@Override
	public float getLabelXOffset(Object agent)
	{
		return -7.0f;
	}
}
