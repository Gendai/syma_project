package district_simul.tiles;

import district_simul.GenericTile;
import repast.simphony.context.Context;
import repast.simphony.space.grid.Grid;

public class Crosswalk extends GenericTile{

	protected TrafficLight associatedTrafficLight;
	protected int order = 0;
	
	public Crosswalk(Grid<GenericTile> gridCopy, Context<GenericTile> contextCopy) {
		super(gridCopy, contextCopy);
	}
	
	public TrafficLight getAssociatedTrafficLight() {
		return associatedTrafficLight;
	}
	
	public int getOrder()
	{
		return this.order;
	}

	public void setAssociatedTrafficLight(TrafficLight associatedTrafficLight, int order) {
		this.associatedTrafficLight = associatedTrafficLight;
		this.order = order;
	}

}
