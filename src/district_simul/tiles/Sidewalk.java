package district_simul.tiles;

import district_simul.GenericTile;
import district_simul.agents.Pedestrians;
import repast.simphony.context.Context;
import repast.simphony.space.grid.Grid;

public class Sidewalk extends GenericTile{

	public Sidewalk(Grid<GenericTile> gridCopy, Context<GenericTile> contextCopy) {
		super(gridCopy, contextCopy);
	}
	
	public String getPedestriansCount()
	{
		repast.simphony.space.grid.GridPoint gp = this.grid.getLocation(this);
		Iterable<GenericTile> gts = this.grid.getObjectsAt(gp.getX(), gp.getY());
		int nb = 0;
		
		for (GenericTile gt : gts)
			if (gt instanceof Pedestrians)
				nb++;
		
		if(nb > 0)
			return "" + nb;
		else
			return "";
	}

}
