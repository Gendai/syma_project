package district_simul.tiles;

import java.util.ArrayList;

import district_simul.GenericTile;
import repast.simphony.context.Context;
import repast.simphony.space.grid.Grid;

public class Road extends GenericTile{

	public enum Direction {NORTH, SOUTH, EAST, WEST};
	
	protected ArrayList<Direction> roadDirections;
	private String mapRepr;
	
	public Road(Grid<GenericTile> gridCopy, Context<GenericTile> contextCopy, ArrayList<Direction> roadDirections, String mapRepr)
	{
		super(gridCopy, contextCopy);
		this.roadDirections = roadDirections;
		this.mapRepr = mapRepr;
	}
	
	public ArrayList<Direction> getRoadDirections() 
	{
		return roadDirections;
	}
	
	public String getMapRepr()
	{
		return this.mapRepr;
	}
	
}
