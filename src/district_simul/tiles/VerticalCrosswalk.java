package district_simul.tiles;

import district_simul.GenericTile;
import repast.simphony.context.Context;
import repast.simphony.space.grid.Grid;

public class VerticalCrosswalk extends Crosswalk{

	public VerticalCrosswalk(Grid<GenericTile> gridCopy, Context<GenericTile> contextCopy) {
		super(gridCopy, contextCopy);
	}

}
