package district_simul.tiles;

import java.security.InvalidParameterException;

import district_simul.GenericTile;
import district_simul.utils.GridPoint;
import repast.simphony.context.Context;
import repast.simphony.space.grid.Grid;

public class BusStop extends Sidewalk{

	private int lineId;
	private int stopId;
	private char dir;

	public BusStop(Grid<GenericTile> gridCopy, Context<GenericTile> contextCopy, int lineId, char dir) {
		super(gridCopy, contextCopy);
		this.lineId = lineId;
		this.dir = dir;
	}
	
	public int getLineId() {
		return lineId;
	}
	
	public int getStopId()
	{
		return stopId;
	}
	
	public void setStopId(int id)
	{
		this.stopId = id;
	}
	
	public char getDir()
	{
		return dir;
	}
	
	public GridPoint GetRoad()
	{
		int x = this.grid.getLocation(this).getX();
		int y = this.grid.getLocation(this).getY();
		switch (dir)
		{
		case 'n':
			return new GridPoint(x, y + 1);
		case 's':
			return new GridPoint(x, y - 1);
		case 'e':
			return new GridPoint(x + 1, y);
		case 'w':
			return new GridPoint(x - 1, y);
		default:
			throw new InvalidParameterException("This destination is not connected to any roads");
		}
	}

}
