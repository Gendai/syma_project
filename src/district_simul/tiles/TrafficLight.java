package district_simul.tiles;

import java.util.ArrayList;

import district_simul.ContextCreator;
import district_simul.GenericTile;
import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.grid.Grid;

public class TrafficLight extends Sidewalk{

	public enum LightState {RED, GREEN};
	
	private LightState lightState = LightState.GREEN;
	private long tickCounter = 1;
	private char dir;
	private ArrayList<Crosswalk> associatedCrosswalks;
	
	public TrafficLight(Grid<GenericTile> gridCopy, Context<GenericTile> contextCopy, char dir) {
		super(gridCopy, contextCopy);
		this.dir = dir;
		this.associatedCrosswalks = new ArrayList<>();
	}

	public LightState getLightState() {
		return lightState;
	}

	public void setLightState(LightState lightState) {
		this.lightState = lightState;
	}
	
	@ScheduledMethod(start = 1, interval = 1, priority = 3)
	public void ChangeState()
	{
		LightState ls = ContextCreator.tlm.GetLightState(grid, this, tickCounter);
		if (!ls.equals(lightState))
		{
			tickCounter = 1;
			this.lightState = ls;
		}
		else
			tickCounter++;
	}
	
	public long tickCounterGet()
	{
		return this.tickCounter;
	}
	
	@Override
	public String getPedestriansCount()
	{
		return "";
	}
	
	public char getDir()
	{
		return dir;
	}
	
	public void AddCrosswalk(Crosswalk cw)
	{
		this.associatedCrosswalks.add(cw);
	}
	
	public void RemoveCrosswalk(Crosswalk cw)
	{
		this.associatedCrosswalks.remove(cw);
	}
	
	public boolean TimeToCross()
	{
		return ContextCreator.tlm.TimeToCross(this, tickCounter);
	}
	
	public int CrosswalkSize()
	{
		return this.associatedCrosswalks.size();
	}
}
